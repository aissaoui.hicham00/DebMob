package fr.epsi.debmob

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity: AppCompatActivity() {
    fun setTitle(Title:String){
        val title: TextView = findViewById(R.id.testid1)
        title.setText(Title)
    }
    fun showButton(){
        val setImageView:ImageView=findViewById(R.id.shbutton)
        setImageView.visibility=View.VISIBLE
        setImageView.setOnClickListener(View.OnClickListener { finish() })

    }

}