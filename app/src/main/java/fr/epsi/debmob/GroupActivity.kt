package fr.epsi.debmob

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button


class GroupActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group)
        val button1:Button = findViewById(R.id.Etudiant1)
        val button2:Button = findViewById(R.id.Etudiant2)
        val button3:Button = findViewById(R.id.Etudiant3)
        showButton()
        setTitle("Infos")

        button1.setOnClickListener(View.OnClickListener {
            EtudiantActivity.startEtudiantActivity(this,"AISSAOUI"
                ,"Hicham"
                ,"hicham.aissaoui@epsi.fr"
                ,"G2"
            ,"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Tony_stark_%283261723587%29.jpg/450px-Tony_stark_%283261723587%29.jpg")




        })
        button2.setOnClickListener(View.OnClickListener {
            EtudiantActivity.startEtudiantActivity(this,"JOHN"
                ,"Jack"
                ,"jack.john@epsi.fr"
                ,"G2"
            ,"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Tony_stark_%283261723587%29.jpg/450px-Tony_stark_%283261723587%29.jpg")





        })


        button3.setOnClickListener(View.OnClickListener {
            EtudiantActivity.startEtudiantActivity(this,"MIKE"
                ,"Leo"
                ,"leo.mike@epsi.fr"
                ,"G2"
            ,"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Tony_stark_%283261723587%29.jpg/450px-Tony_stark_%283261723587%29.jpg")




        })






    }









    companion object{
        fun startGroupActivity(con: Context){
            con.startActivity(Intent(con,GroupActivity::class.java))
        }
    }






}