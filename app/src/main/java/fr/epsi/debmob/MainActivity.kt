package fr.epsi.debmob

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val zone1:Button=findViewById(R.id.zone1)
        val zone2:Button=findViewById(R.id.zone2)
        zone1.setOnClickListener(View.OnClickListener { GroupActivity.startGroupActivity(this) })
        zone2.setOnClickListener(View.OnClickListener { CategorieActivity.startCategorieActivity(this) })
        setTitle("EPSI")
    }

        companion object {fun startMainActivity(con: Context){
        con.startActivity(Intent(con,MainActivity::class.java))
    }


    }
}